FROM python:3.9-slim

WORKDIR /src/petvet

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY clinic .

# Run database migrations
COPY docker-entrypoint.sh .
ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["gunicorn", "clinic.wsgi"]

EXPOSE 9000
